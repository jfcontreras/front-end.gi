export class Customer{

    id: number
    firstName: string
    lastName: string
    emailId: string
    phone: string
    description: string
    document: Document
    
}
