import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomerComponent } from './customer/customer.component';
import { DocumentComponent } from './document/document.component';
import { DialogCustomerComponent } from './customer/dialog-customer/dialog-customer.component';
import { DialogDocumentComponent } from './document/dialog-document/dialog-document.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {MatTooltipModule} from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatTabsModule} from '@angular/material/tabs';
import {MatTableModule} from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatPaginatorModule} from '@angular/material/paginator';
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import {MatSortModule} from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { GenericService } from './services';


@NgModule({
  declarations: [
    AppComponent,
    CustomerComponent,
    DialogCustomerComponent,
    DialogDocumentComponent,
    DocumentComponent
  ],
  imports: [
    BrowserModule, 
    MatButtonModule,
    MatInputModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatTabsModule,
    MatGridListModule,
    MatInputModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatSlideToggleModule,
    MatCheckboxModule,
    MatSelectModule,
    MatDialogModule,
    MatSidenavModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    NgxUiLoaderModule,
    FormsModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
    ToastContainerModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
  })
  ],
  providers: [GenericService],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogCustomerComponent,
    DialogDocumentComponent
  ]
})
export class AppModule { }


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
