import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { GenericService } from 'src/app/services';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Document } from 'src/app/models/document';
import { Customer } from 'src/app/models/customer';

@Component({
  selector: 'app-dialog-customer',
  templateUrl: './dialog-customer.component.html',
  styleUrls: ['./dialog-customer.component.scss']
})
export class DialogCustomerComponent {
  documents;
  dataForm: FormGroup;
  imagesHouse = [];
  selectedBrand = false;
  textMode = '';

  /*id: number
  firstName: string
  lastName: string
  emailId: string
  phone: string
  description: string
  document: Document*/

  constructor(public dialogRef: MatDialogRef<DialogCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _exampleDatabase: GenericService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private ngxService: NgxUiLoaderService,
    public dataService: GenericService) {
    this._exampleDatabase.getAllSelect('/v1/document?pageable=false').subscribe(data => {
      this.documents = data.content;
    },
      (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
      });

    console.log('*********', data);
    if (data.mode === 'create') {
      this.dataForm = this.formBuilder.group({
        id: ['', Validators.required],
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        emailId: ['', [Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
        phone: ['', [Validators.required,Validators.pattern('^[3]{1}[0-9]{9}$'),Validators.minLength(10)]],
        document:['', Validators.required]
      });
      this.dataForm.controls['id'].disable();
    } else {
      this.dataForm = this.formBuilder.group({
        id: [data.data.id, Validators.required],
        firstName: [data.data.firstName, Validators.required],
        lastName: [data.data.lastName, Validators.required],
        emailId: [data.data.emailId, [Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
        phone:[data.data.phone, [Validators.required,Validators.pattern('^[3]+[0-9]{8}$'),Validators.minLength(10)]],
        document:[data.data.document.id, Validators.required]
      });
      this.selectedBrand = true;
      this.dataForm.controls['id'].disable();
    }

    if(this.data.mode === 'delete'){
      this.dataForm.controls['firstName'].disable();
      this.dataForm.controls['lastName'].disable();
      this.dataForm.controls['emailId'].disable();
      this.dataForm.controls['phone'].disable();
      this.dataForm.controls['document'].disable();
     this.textMode = 'ELIMINAR';
    }else{
      if(this.data.mode === 'edit'){
        this.textMode = 'EDITAR';
      }else{
        if(this.data.mode === 'create'){
          this.textMode = 'CREAR';
        }
      }
    }

  }



  onOptionsSelected(v) {
    /*let cb = new Document();
    cb.id = v.value;
    console.log(v.value);*/
    //this.data.brand.categoryBrand = cb;
  }

  eventUploadFiles(count: any) {
    this.selectedBrand = true;
    this.data.brand.code = count;
  }

  getErrorMessage() {
    /*return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';*/
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    this.selectedBrand = false;
    let mode = 0;
    if(this.data.mode === 'delete'){
      mode = 3;
      this.dataService.delete(`/v1/customer/${this.data.data.id}`).subscribe(
        data => {
          this.ngxService.stop();
          this.dialogRef.close({data: data, indice : this.data.indice});
          console.log(data)
        },
        (error) => {
          this.ngxService.stop();
          console.log(error)
        }
      )
    }else{
      if(this.data.mode === 'edit'){
        mode = 2;
          /*id: number
  firstName: string
  lastName: string
  emailId: string
  phone: string
  description: string
  document: Document*/
        this.data.data.firstName = this.dataForm.controls.firstName.value;
        this.data.data.lastName = this.dataForm.controls.lastName.value;
        this.data.data.emailId = this.dataForm.controls.emailId.value;
        this.data.data.phone = this.dataForm.controls.phone.value;
        this.data.data.document = new Document();
        this.data.data.document.id = this.dataForm.controls.document.value;
        this.dataService.putRequest(`/v1/customer/${this.data.data.id}`,this.data.data).subscribe(
          data => {
            this.ngxService.stop();
            this.dialogRef.close({data: data, indice : this.data.indice});
            console.log(data)
          },
          (error) => {
            this.ngxService.stop();
            console.log(error)
          }
        )
      }else{
        if(this.data.mode === 'create'){
          mode = 1;
          this.data.data.firstName = this.dataForm.controls.firstName.value;
          this.data.data.lastName = this.dataForm.controls.lastName.value;
          this.data.data.emailId = this.dataForm.controls.emailId.value;
          this.data.data.phone = this.dataForm.controls.phone.value;
          this.data.data.document = new Document();
          this.data.data.document.id = this.dataForm.controls.document.value;

          this.dataService.addGeneric(this.data.data, '/v1/customer').subscribe(
            data =>{
              this.ngxService.stop();
              this.dialogRef.close({data: data, indice : this.data.indice});
            },
            (error) => {
              console.log(error)
            }
          )
        }
      }
    }
  }
}
