import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MatDialog, MatDialogConfig, } from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { GenericService } from '../services';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent } from 'rxjs';
import { map, catchError, startWith, switchMap } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { merge, Observable, of as observableOf } from 'rxjs';
import { environment } from '../../environments/environment'
import { DialogCustomerComponent } from './dialog-customer/dialog-customer.component';
import { Customer } from '../models/customer';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements AfterViewInit  {

 
  /*id: number
  firstName: string
  lastName: string
  emailId: string
  phone: string
  description: string
  document: Document*/

  displayedColumns: string[] = ['id', 'firstName', 'lastName','emailId','phone','document','actions'];
  genericDatabase: GenericService | null;
  data: any;

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private httpClient: HttpClient,
    public dialog: MatDialog,
    private ngxService: NgxUiLoaderService) { }

  ngAfterViewInit() {
    this.genericDatabase = new GenericService(this.httpClient, this.ngxService);

    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.reloadData();
  }

  reloadData() {
    merge(this.sort.sortChange, this.paginator.page)
    .pipe(
      startWith({}),
      switchMap(() => {
        this.ngxService.start();
        this.isLoadingResults = true;
        return this.genericDatabase.getGenericData(
          this.sort.active, this.sort.direction, this.paginator.pageIndex, this.paginator.pageSize, `/v1/customer`);
      }),
      map(data => {
        // Flip flag to show that loading has finished.
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.resultsLength = data.totalElements;
        this.ngxService.stop();
        return data.content;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        this.ngxService.stop();
        // Catch if the GitHub API has reached its rate limit. Return empty data.
        this.isRateLimitReached = true;
        return observableOf([]);
      })
    ).subscribe(data =>
      this.data = data
    );
  }

  encodeNameImage(value) {
    let enco = encodeURI(value);
    enco = environment.apiUrl+'/v1/images/files/' + enco + '.jpg';
    return enco;
  }

  addNew() {
    const config = {
      position: {
        top: '20px'
      },
      height: '90%',
      width: '50vw',
      data: { data: new Customer(), mode: 'create' },
      panelClass: 'full-screen-modal',
    };
    const dialogRef = this.dialog.open(DialogCustomerComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.data.id) {
        this.reloadData();
      }
    });
  }

  startEdit(index, data) {
    console.log(data);
    // this.id = id;
    // index row is used just for debugging proposes and can be removed
    // this.index = i;
    const config = {
      position: {
        top: '20px'
      },
      height: '90%',
      width: '50vw',
      data: {},
      panelClass: 'full-screen-modal',
    };
    config.data = { data: { ...data }, mode: 'edit', indice: index }
    const dialogRef = this.dialog.open(DialogCustomerComponent, config);

    dialogRef.afterClosed().subscribe(result => {
      if (result && result.data.id) {
        this.reloadData();

      }
    });
  }

  applyFilter(filterValue: string) {
    this.data.filter = filterValue.trim().toLowerCase();

    if (this.data.paginator) {
      this.data.paginator.firstPage();
    }
  }

  deleteItem(index, data) {
    const config = {
      position: {
        top: '20px'
      },
      height: '90%',
      width: '50vw',
      data: {},
      panelClass: 'full-screen-modal',
    };
    config.data = { data: { ...data }, mode: 'delete', indice: index }
    const dialogRef = this.dialog.open(DialogCustomerComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result) {
        this.reloadData();
      }
    });
  }

}
