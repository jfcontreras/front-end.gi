import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentComponent } from './document/document.component';
import { CustomerComponent } from './customer/customer.component';


const routes: Routes = [
  {
    path: 'document',
    component: DocumentComponent
  },
  {
    path: 'customer',
    component: CustomerComponent,
  },
  {
    path: '',
    redirectTo: 'customer',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
