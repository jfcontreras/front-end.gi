import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { GenericService } from 'src/app/services';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Document } from 'src/app/models/document';

@Component({
  selector: 'app-dialog-document',
  templateUrl: './dialog-document.component.html',
  styleUrls: ['./dialog-document.component.scss']
})
export class DialogDocumentComponent {
  dataForm: FormGroup;
  imagesHouse = [];
  textMode = '';
  constructor(public dialogRef: MatDialogRef<DialogDocumentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public _exampleDatabase: GenericService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private ngxService: NgxUiLoaderService,
    public dataService: GenericService) {
    if (data.mode === 'create') {
      this.dataForm = this.formBuilder.group({
        id: ['', Validators.required],
        name: ['', Validators.required],
        description: ['', Validators.required]
      });
      this.dataForm.controls['id'].disable();
    } else {
      this.dataForm = this.formBuilder.group({
        id: [data.data.id, Validators.required],
        name: [data.data.name, Validators.required],
        description: [data.data.description, Validators.required]
      });
      this.dataForm.controls['id'].disable();
    }

    if(this.data.mode === 'delete'){
      this.dataForm.controls['name'].disable();
      this.dataForm.controls['description'].disable();
     this.textMode = 'ELIMINAR';
    }else{
      if(this.data.mode === 'edit'){
        this.textMode = 'EDITAR';
      }else{
        if(this.data.mode === 'create'){
          this.textMode = 'CREAR';
        }
      }
    }

  }



  onOptionsSelected(v) {
    let cb = new Document();
    cb.id = v.value;
    console.log(v.value);
    this.data.brand.categoryBrand = cb;
  }

  eventUploadFiles(count: any) {
    this.data.brand.code = count;
  }

  getErrorMessage() {
    /*return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';*/
  }

  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    let mode = 0;
    if(this.data.mode === 'delete'){
      mode = 3;
      this.dataService.delete(`/v1/document/${this.data.data.id}`).subscribe(
        data => {
          this.ngxService.stop();
          this.dialogRef.close({data: data, indice : this.data.indice});
          console.log(data)
        },
        (error) => {
          this.ngxService.stop();
          console.log(error)
        }
      )
    }else{
      if(this.data.mode === 'edit'){
        mode = 2;
        this.data.data.name = this.dataForm.controls.name.value;
        this.data.data.description = this.dataForm.controls.description.value;
        this.dataService.putRequest(`/v1/document/${this.data.data.id}`,this.data.data).subscribe(
          data => {
            this.ngxService.stop();
            this.dialogRef.close({data: data, indice : this.data.indice});
            console.log(data)
          },
          (error) => {
            this.ngxService.stop();
            console.log(error)
          }
        )
      }else{
        if(this.data.mode === 'create'){
          mode = 1;
          this.data.data.name = this.dataForm.controls.name.value;
          this.data.data.description = this.dataForm.controls.description.value;
          this.dataService.addGeneric(this.data.data, '/v1/document').subscribe(
            data =>{
              this.ngxService.stop();
              this.dialogRef.close({data: data, indice : this.data.indice});
            },
            (error) => {
              console.log(error)
            }
          )
        }
      }
    }
  }
}