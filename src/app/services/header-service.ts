import { Injectable, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class HeaderService {

    skipClicked: EventEmitter<any> = new EventEmitter();
    loadMenu: EventEmitter<any> = new EventEmitter();
    showData: EventEmitter<any> = new EventEmitter();
    notifications: EventEmitter<any> = new EventEmitter();
    updateDevices: EventEmitter<any> = new EventEmitter();
    changeChart: EventEmitter<any> = new EventEmitter();

    
    constructor() {
    }

    setClicked(value) {
        console.log('clicked');
        this.skipClicked.emit(value);
    };

    setLoadMenu(value){
        this.loadMenu.emit(value);
    }

    setValuesFromBuble(value){
        this.showData.emit(value);
    }

    updateNotifications(value){
        this.notifications.emit(value);
    }

    updateDevicesChart(value){
        this.updateDevices.emit(value);
    }

    changedChart(value){
        this.changeChart.emit(value);
    }

}