import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {NgxUiLoaderService} from 'ngx-ui-loader';

@Injectable()
export class GenericService {

  dataChange: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;
  httpOptionsjson = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  get data(): any[] {
    return this.dataChange.value;
  }

  getPDF(service, accept, body) {
    let json = JSON.stringify(body);
    // this.httpOptionsjson.headers.append('responseType', 'blob');
    // this.httpOptionsjson.headers.append('Accept', accept);
    // return this.http.get<any>(, this.httpOptionsjson);
    /*return this.http
              .get(environment.apiUrl + service, this.httpOptionsjson)
              .subscribe(data => {
               console.log(data);
                },
                (error: HttpErrorResponse) => {
                 console.log('error'+ error);
                });*/
    return this.http.post(environment.apiUrl + service, json, {
      headers: this.httpOptionsjson.headers,
      observe: 'response',
      responseType: 'text'
    });
  }

  getVideo(service, accept) {
    this.httpOptionsjson.headers.append('Accept', accept);
    console.log(service, accept, 'peticion');
    return this.http.get(environment.apiUrl + service, {
      headers: this.httpOptionsjson.headers,
      observe: 'response',
      responseType: 'blob'
    });
  }

  constructor(private http: HttpClient, private ngxService: NgxUiLoaderService) {
  }


  getDialogData() {
    return this.dialogData;
  }

  getByParams(sort: string, order: string, page: number, size: number, service: string, params: string): Observable<any> {
    const href = environment.apiUrl + service;
    const requestUrl =
      `${href}?sort=${sort}&order=${order}&page=${page}&size=${size}${params}`;
    return this.http.get<any>(requestUrl);
  }

  getGenericData(sort: string, order: string, page: number, size: number, service: string): Observable<any> {
    const href = environment.apiUrl + service;
    const requestUrl =
      `${href}?sort=${sort}&order=${order}&page=${page}&size=${size}`;
    return this.http.get<any>(requestUrl);
  }

  getGenericDataSecurityAdmin(sort: string, order: string, page: number, size: number, service: string): Observable<any> {
    const href = environment.adminUrl + service;
    const requestUrl =
      `${href}?sort=${sort}&order=${order}&page=${page}&size=${size}`;
    return this.http.get<any>(requestUrl);
  }

  addSecurityAdmin(data: any, service: String) {
    let json = JSON.stringify(data);
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<any>(environment.adminUrl + service, json, httpOptions);
  }


  getSelectSecurityAdmin(service) {
    return this.http.get<any>(environment.adminUrl + service, this.httpOptionsjson);
  }


  getAllTest(service): void {
    this.ngxService.start();
    this.http.get<any[]>(environment.apiUrl + service, this.httpOptionsjson).subscribe(data => {
        this.ngxService.stop();
        this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
        this.ngxService.stop();
        console.log(error.name + ' ' + error.message);
      });
  }


  getAllSelect(service) {
    return this.http.get<any>(environment.apiUrl + service, this.httpOptionsjson);
  }

  putRequest(service, data: any) {
    let json = JSON.stringify(data);
    return this.http.put(environment.apiUrl + service, json, this.httpOptionsjson);
  }
  

  getAllSimple(service) {
    return this.http.get<any>(environment.apiUrl + service, this.httpOptionsjson);
  }


  getGenericDataUnsort(page: number, size: number, user: string, rol: number, status: number, service: string): Observable<any> {
    const href = environment.apiUrl + service;

    if (!rol) {
      rol = -1;
    }
    if (!status) {
      status = -1;
    }

    const requestUrl =
      `${href}?page=${page}&size=${size}&user=${user}&rol=${rol}&status=${status}`;
    return this.http.get<any>(requestUrl);
  }

  getAll() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${environment.apiUrl}`, httpOptions);
  }

  /*getById(id: number) {
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
  }*/

  addObject(data: any, service: String) {
    console.log(data);
    let httpOptions = {
      headers: new HttpHeaders({})
    };
    return this.http.post(environment.apiUrl + service, data, httpOptions);
  }

  addGeneric(data: any, service: String) {
    let json = JSON.stringify(data);
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<any>(environment.apiUrl + service, json, httpOptions);
  }



  addObjectToJSON(data: any, service: String) {
    this.ngxService.start();
    let json = JSON.stringify(data);
    return this.http.post(environment.apiUrl + service, json, this.httpOptionsjson).subscribe(data => {
        this.ngxService.stop();
        console.log(data);
      },
      (error: HttpErrorResponse) => {
        this.ngxService.stop();
        console.log(error.name + ' ' + error.message);
      });
  }

  get(service: String): any {
    return this.http.get(environment.apiUrl + service, this.httpOptionsjson);
  }

  getCheck(service: String): any {
    return this.http.get(environment.securityUrl + service, this.httpOptionsjson);
  }

  remove(service: String): any {
    return this.http.delete(environment.apiUrl + service, this.httpOptionsjson).subscribe(data => {
        this.ngxService.stop();
        console.log(data);
      },
      (error: HttpErrorResponse) => {
        this.ngxService.stop();
        console.log(error.name + ' ' + error.message);
      });
  }

  delete(service: String): any {
    return this.http.delete(environment.apiUrl + service, this.httpOptionsjson);
  }

  deleteSecurityAdmin(service: String): any {
    return this.http.delete(environment.adminUrl + service, this.httpOptionsjson);
  }

  addObjectCheck(data: String, service: String) {
    console.log(data);
    return this.http.post(environment.securityUrl + service, data, this.httpOptionsjson);
  }
}
